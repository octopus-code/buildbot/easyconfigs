easyblock = 'CMakeMake'

name = 'DFTB+'
version = '21.1'
versionsuffix = '-serial'

homepage = 'https://www.dftb-plus.info'
description = """DFTB+ is a fast and efficient versatile quantum mechanical simulation package.
It is based on the Density Functional Tight Binding (DFTB) method, containing
almost all of the useful extensions which have been developed for the DFTB
framework so far.  Using DFTB+ you can carry out quantum mechanical simulations
like with ab-initio density functional theory based packages, but in an
approximate way gaining typically around two order of magnitude in speed."""

toolchain = {'name': 'foss', 'version': '2021a'}
toolchainopts = {'usempi': False, 'openmp': False, 'pic': True, 'extra_fflags': '-I$(EBROOTDFTD3MINLIB)/include'}

local_external_dir = '%%(builddir)s/dftbplus-%%(version)s/external/%s/origin/'
local_external_extract = 'mkdir -p %s && tar -C %s' % (local_external_dir, local_external_dir)
local_external_extract += ' --strip-components=1 -xzf %%s'

source_urls = ['https://github.com/dftbplus/dftbplus/releases/download/%(version)s']
sources = [
    {
        # DFTB+ source code
        'source_urls': ['https://github.com/dftbplus/dftbplus/archive'],
        'download_filename': '%(version)s.tar.gz',
        'filename': SOURCE_TAR_GZ,
    },
    {
        # Slater-Koster (slakos) data for testing
        'source_urls': ['https://github.com/dftbplus/testparams/archive'],
        'download_filename': '26439495c4b5b0d07be6ceec4dade3d149425465.tar.gz',
        'filename': 'slakos-data-%(version)s.tar.gz',
        'extract_cmd': local_external_extract % ('slakos', 'slakos'),
    },
    {
        # GBSA (gbsa) data for testing
        'source_urls': ['https://github.com/grimme-lab/gbsa-parameters/archive'],
        'download_filename': '6836c4d997e4135e418cfbe273c96b1a3adb13e2.tar.gz',
        'filename': 'gbsa-data-%(version)s.tar.gz',
        'extract_cmd': local_external_extract % ('gbsa', 'gbsa'),
    },
]
patches = [
    '%(name)s-%(version)s_use_dftd3_from_EB.patch',
]
checksums = [
    '31d5a488843a05d8589a375307a2832c1fc938f9f7d830c45a062726659e7b0a',  # DFTB+-21.1.tar.gz
    '6f65da7b64ef255fd777de7a972c63f7fa8ed2041442823b2c601bc8f0e55d98',  # slakos-data-21.1.tar.gz
    'd464f9f7b1883d1353b433d0c7eae2f5606af092d9b51d38e9ed15e072610a79',  # gbsa-data-21.1.tar.gz
    '3bd33d8e90e8182f516f15fb8f79d01929a0778e324b33c5fabc7d70112e643a',  # DFTB+-21.1_use_dftd3_from_EB.patch
]

builddependencies = [
    ('CMake', '3.20.1'),
    ('pkg-config', '0.29.2'),
]

dependencies = [
    ('dftd3-lib', '0.9'),
]

build_shared_libs = True

configopts = '-DWITH_API=True -DWITH_OMP=False -DWITH_MPI=False -DWITH_DFTD3=True -DWITH_ELSI=False -DWITH_MBD=True -DWITH_UNIT_TESTS=False -DWITH_PYTHON=False'
configopts += '-DSCALAPACK_LIBRARY="$LIBSCALAPACK" '

# pyapi tests fail, so we disable running the tests
#runtest = 'test'

sanity_check_paths = {
    'files': [['bin/' + x for x in ['dftb+', 'dp_bands', 'dp_dos', 'gen2cif', 'gen2xyz', 'makecube',
                                    'modes', 'repeatgen', 'straingen', 'waveplot', 'xyz2gen']] +
              ['lib/' + x for x in ['libdftbplus.a', 'libmudpack.a']],
              'include/dftbplus.h'],
    'dirs': ['lib/pkgconfig']
}

moduleclass = 'phys'
