easyblock = 'ConfigureMake'

name = 'FFTW'
version = '3.3.4'
versionsuffix = '-PFFT-20150905'

homepage = 'http://www.fftw.org'
description = """FFTW is a C subroutine library for computing the discrete Fourier transform (DFT)
 in one or more dimensions, of arbitrary input size, and of both real and complex data."""

toolchain = {'name': 'gompi', 'version': '2016b'}
toolchainopts = {'optarch': True, 'pic': True}

source_urls = [homepage]
sources = [SOURCELOWER_TAR_GZ]
patches = ['%(name)s_%(version)s%(versionsuffix)s.patch']
checksums = [
    '8f0cde90929bc05587c3368d2f15cd0530a60b8a9912a8e2979a72dbe5af0982',  # fftw-3.3.4.tar.gz
    'a530c6aee10fb3591d87d84f039238b0988d9848db232e5e97c4c0844f22eb53',  # FFTW_3.3.4-PFFT-20150905.patch
]

builddependencies = [
    ('Autotools', '20150215'),
]

# the patch changed a Makefile.am, so we need to call autoreconf
preconfigopts = "autoreconf --verbose --symlink --force && " 

common_configopts = "--enable-threads --enable-openmp --with-pic"

# no quad precision, requires GCC v4.6 or higher
# see also http://www.fftw.org/doc/Extended-and-quadruple-precision-in-Fortran.html
configopts = [
    common_configopts + " --enable-single --enable-sse2 --enable-mpi",
    common_configopts + " --enable-long-double --enable-mpi",
    common_configopts + " --enable-sse2 --enable-mpi",  # default as last
]

sanity_check_paths = {
    'files': ['bin/fftw%s' % x for x in ['-wisdom', '-wisdom-to-conf', 'f-wisdom', 'l-wisdom']] +
             ['include/fftw3%s' % x for x in ['-mpi.f03', '-mpi.h', '.f', '.f03',
                                              '.h', 'l-mpi.f03', 'l.f03', 'q.f03']] +
             ['lib/libfftw3%s%s.a' % (x, y) for x in ['', 'f', 'l'] for y in ['', '_mpi', '_omp', '_threads']],
    'dirs': ['lib/pkgconfig'],
}

moduleclass = 'numlib'
