#/bin/bash

function usage() {
    cat <<EOF

    Usage: new_toolchain.sh SRC DST

    new_toolchain.sh creates a starting point for the Octopus buildbot specific
    easyconfigs using an existing set of easyconfigs.

    The scrip will copy the existing files and, whenever possible, rename them
    and replace the toolchain version inside the file.

    The script can only rename and replace files whose toolchains version is of
    the form YYYY{a,b}. That means it will not replace or rename easyconfigs for
    the GCC or intelcompilers toolchains. It will also not change the versions
    of any dependencies. This means that some manual editing is still required.

EOF
    exit 0;
}


# sanity checks
if [ -z "$1" ]; then
    echo "Error: No source toolchain provided"
    usage
    exit 1
fi
if [ -z "$2" ]; then
    echo "Error: No destination toolchain provided"
    usage
    exit 1
fi

SRC=$(echo $1 | sed 's:/*$::')
DST=$(echo $2 | sed 's:/*$::')

if [ ! -d $SRC ]; then
    echo "Error: $SRC directory does not exist"
    usage
    exit 1
fi
if [ -d $DST ]; then
    echo "Error: $DST directory already exist"
    usage
    exit 1
fi

# TODO: check that the source and destination directories are of the form YYYY{a,b}


# Copy all files
cp -r $SRC $DST

# Rename easyconfigs
pushd $DST
for file in $(ls */*/*.eb); do
    new_file=$(echo $file | sed -r "s/$SRC/$DST/")
    if [ "$file" != "$new_file" ]; then
        mv $file $new_file
    fi
    sed -i "s/$SRC/$DST/g" $new_file
done
popd
