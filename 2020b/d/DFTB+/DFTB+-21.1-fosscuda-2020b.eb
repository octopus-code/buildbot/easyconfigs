easyblock = 'CMakeMake'

name = 'DFTB+'
version = '21.1'

homepage = 'https://www.dftb-plus.info'
description = """DFTB+ is a fast and efficient versatile quantum mechanical simulation package.
It is based on the Density Functional Tight Binding (DFTB) method, containing
almost all of the useful extensions which have been developed for the DFTB
framework so far.  Using DFTB+ you can carry out quantum mechanical simulations
like with ab-initio density functional theory based packages, but in an
approximate way gaining typically around two order of magnitude in speed."""

toolchain = {'name': 'fosscuda', 'version': '2020b'}
toolchainopts = {'usempi': True, 'pic': True}

local_external_dir = '%%(builddir)s/dftbplus-%%(version)s/external/%s/origin/'
local_external_extract = 'mkdir -p %s && tar -C %s' % (local_external_dir, local_external_dir)
local_external_extract += ' --strip-components=1 -xzf %%s'

sources = [
    {
        # DFTB+ source code
        'source_urls': ['https://github.com/dftbplus/dftbplus/archive'],
        'download_filename': '%(version)s.tar.gz',
        'filename': SOURCE_TAR_GZ,
    },
    {
        # Slater-Koster (slakos) data for testing
        'source_urls': ['https://github.com/dftbplus/testparams/archive'],
        'download_filename': 'd0ea16df2b56d14c7c3dc9329a8d3bac9fea50a0.tar.gz',
        'filename': 'slakos-data-%(version)s.tar.gz',
        'extract_cmd': local_external_extract % ('slakos', 'slakos'),
    },
]
checksums = [
    '31d5a488843a05d8589a375307a2832c1fc938f9f7d830c45a062726659e7b0a',  # DFTB+-21.1.tar.gz
    '6f65da7b64ef255fd777de7a972c63f7fa8ed2041442823b2c601bc8f0e55d98',  # slakos-data-21.1.tar.gz
]

builddependencies = [('CMake', '3.18.4')]

configopts = '-DWITH_API=True -DWITH_MPI=True -DWITH_OMP=False -DSCALAPACK_LIBRARY=scalapack'

#runtest = 'test'

sanity_check_paths = {
    'files': [['bin/' + x for x in ['dftb+', 'integvalue', 'modes', 'polyvalue', 'skderivs', 'splvalue', 'waveplot']] +
              ['lib/' + x for x in ['libdftbplus.a', 'libmpifx.a', 'libmudpack.a', 'libscalapackfx.a']],
              'include/dftbplus.h'],
    'dirs': ['lib/pkgconfig']
}

moduleclass = 'phys'
